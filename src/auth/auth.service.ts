// src/auth/auth.service.ts
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcryptjs';
import { loginDto } from './dto/login.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(loginDto:loginDto) : Promise<any>{
    const user = await this.usersService.findOne(loginDto.email);
    if (!user) {
      throw new HttpException("User not found", HttpStatus.NOT_FOUND)
    }
    const match = await bcrypt.compare(loginDto.password, user.password);
    if(!match){
      throw new HttpException("Password incorrect", HttpStatus.UNAUTHORIZED)
    }
    const payload = { email: user.email, sub: user._id,username:user.username,userId:user.userId };
    return {
        data:payload,
        access_token: this.jwtService.sign(payload),
      };
  }

}
