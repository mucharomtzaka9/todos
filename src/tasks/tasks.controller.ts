import { Controller, Post, Body, UseGuards, Get,Request, Param, Delete, Patch } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger';
import { TasksDto } from './dto/tasks.dto';
import { TasksService } from './tasks.service';
import { DeleteTasksResponse } from './dto/delete-response.dto';
import { FindOneParams } from './dto/find-one-params.dto';
import { Tasks } from './schemas/tasks.schema';
import { UpdateTaskDto } from './dto/update-task.dto';

@Controller('tasks')
export class TasksController {
    constructor(private readonly taskService: TasksService) {}

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Get('show/all/:userId')
    @ApiParam({name:'userId',description:'userId'})
    async taskall(@Param() {userId}){
       return this.taskService.findAll(userId);
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Post('add')
    async addtask(@Body() tasksDto:TasksDto){
        return this.taskService.create(tasksDto);
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Patch(':taskId')
    @ApiParam({name: 'taskId', required: true, description: 'either an integer for the task id or a string for the jobs name , example: 535fedd2-ac81-4dcc-80ca-a5287d549e90', schema: { oneOf: [{type: 'string'}, {type: 'integer'}]}})
    @ApiOperation({
        description: 'Update a task by taskId.',
    })
    @ApiOkResponse({
        description: 'The task was successfully updated.',
        type: Tasks,
    })
    async update(
        @Param() { taskId }: FindOneParams,
        @Body() updateTaskDto: UpdateTaskDto,
    ): Promise<Tasks> {
        return this.taskService.updateByIdTask(taskId, updateTaskDto);
    }


    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Patch(':taskId/status/:status')
    @ApiParam(
        {name: 'taskId',
        required: true,
        description: 'either an integer for the task id or a string for the jobs name , example: 535fedd2-ac81-4dcc-80ca-a5287d549e90',
         schema: { oneOf: [{type: 'string'}, {type: 'integer'}]
      },
    })
    @ApiParam({name:'status',type:'boolean',
      required: true,
      description :' true or false'
    })
    @ApiOperation({
        description: 'Update status a task by taskId.',
    })
    @ApiOkResponse({
        description: 'The task was successfully updated.',
        type: Tasks,
    })
    async updateTaskStatusEdit(
        @Param() { taskId,status },
    ): Promise<Tasks> {
        return this.taskService.updateByIdTaskStatus(taskId,status);
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt')
    @Delete(':taskId')
    @ApiOperation({
      description: 'Delete a task by taskid.',
    })
    @ApiOkResponse({
      description: 'The tasks was successfully deleted.',
      type: DeleteTasksResponse,
    })
    async deleteById(@Param() { taskId }: FindOneParams): Promise<DeleteTasksResponse> {
      return this.taskService.removetask(taskId);
    }
}
