// src/users/schemas/user.schema.ts
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { randomUUID } from 'crypto';

export type TasksDocument = Tasks & Document;

@Schema({
  toJSON: {
    // Hide the  field when returning user data as JSON
    transform: (doc, ret) => {
      delete ret.__v;
      delete ret._id;
      return ret;
    },
  },
  toObject: {
    // Hide the  field when converting user data to a plain object
    transform: (doc, ret) => {
      delete ret.__v;
      delete ret._id;
      return ret;
    },
  },
})
export class Tasks {

  @ApiProperty({
    example: '8104f19c-a2d8-40f7-9a0b-12f4c6a4b80a',
    description: 'The ID of the tasks',
  })
  @Prop({
    required: true,
    index: { unique: true },
    default: () => randomUUID(),
  })
  taskId: string;

  @ApiProperty({
    example: 'Jobs',
    description: 'The jobs of the user',
  })
  @Prop({ required: true, trim: true, lowercase: true })
  title: string;

  @ApiProperty({
    example: 'mucharom@gmail.com',
    description: 'The email of the user',
  })
  @Prop({
    required: true,
  })
  description: string;

  @Prop({ required: true })
  status: boolean;

  @ApiProperty({
    example: '8104f19c-a2d8-40f7-9a0b-12f4c6a4b80a',
    description: 'The ID of the user',
  })
  @Prop({
    required: true,
  })
  userId: string;
}

export const TasksSchema = SchemaFactory.createForClass(Tasks);
