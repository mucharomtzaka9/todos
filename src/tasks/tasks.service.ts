import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AnyArray, Model } from 'mongoose';
import { Tasks, TasksDocument } from './schemas/tasks.schema';
import { TasksDto } from './dto/tasks.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { DeleteTasksResponse } from './dto/delete-response.dto';

@Injectable()
export class TasksService {
    constructor(@InjectModel(Tasks.name) private taskModel: Model<TasksDocument>) {}

    async create(tasksDto:TasksDto) : Promise<Tasks | any> {
        const createtask =  new this.taskModel({...tasksDto,status:false});
        return createtask.save();
    }

    async findAll(userIdx:string) : Promise<Tasks[]>{
        return this.taskModel.find({userId:userIdx}).exec(); 
    }

    async findOne(titlex: string,userIdx:string): Promise<Tasks|any> {
        return this.taskModel.findOne({ title:titlex,userId:userIdx }).exec();
    }

    async updateByIdTask(taskId : string,taskUpdate:UpdateTaskDto) : Promise<Tasks>{
        return this.taskModel.findOneAndUpdate({taskId},taskUpdate,{
            new:true
        }).exec();
    }

    async updateByIdTaskStatus(taskId : string,status_task:boolean) : Promise<Tasks>{
        return this.taskModel.findOneAndUpdate({taskId},{status:status_task},{
            new:true
        }).exec();
    }

    async removetask(taskId:string):Promise<DeleteTasksResponse>{
        return this.taskModel.deleteOne({taskId}).exec();
    }

}
