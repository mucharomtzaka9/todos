import { PartialType } from '@nestjs/swagger';
import { TasksDto } from './tasks.dto';
export class UpdateTaskDto extends PartialType(TasksDto) {}