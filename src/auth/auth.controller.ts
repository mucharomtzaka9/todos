// src/auth/auth.controller.ts
import { Controller, Post, Body, UseGuards} from '@nestjs/common';
import { AuthService } from './auth.service';
import { loginDto } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('login')
  async login(@Body() loginDto : loginDto) {
    return this.authService.login(loginDto);
  }
}
