// src/users/schemas/user.schema.ts
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { randomUUID } from 'crypto';

export type UserDocument = User & Document;

@Schema({
  toJSON: {
    // Hide the password field when returning user data as JSON
    transform: (doc, ret) => {
      delete ret.password;
      delete ret.__v;
      delete ret._id;
      return ret;
    },
  },
  toObject: {
    // Hide the password field when converting user data to a plain object
    transform: (doc, ret) => {
      delete ret.password;
      delete ret.__v;
      delete ret._id;
      return ret;
    },
  },
})
export class User {

  @ApiProperty({
    example: '8104f19c-a2d8-40f7-9a0b-12f4c6a4b80a',
    description: 'The ID of the user',
  })
  @Prop({
    required: true,
    index: { unique: true },
    default: () => randomUUID(),
  })
  userId: string;

  @ApiProperty({
    example: 'Mucharom',
    description: 'The username of the user',
  })
  @Prop({ required: true, trim: true, lowercase: true })
  username: string;

  @ApiProperty({
    example: 'mucharom@gmail.com',
    description: 'The email of the user',
  })
  @Prop({
    required: true,
    index: { unique: true },
    lowercase: true,
  })
  email: string;

  @Prop({ required: true })
  password: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
