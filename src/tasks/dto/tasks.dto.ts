import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class TasksDto{

  @ApiProperty({
    example: 'title jobs',
      description: 'The jobs of the user',
    })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty({
    example: ' Description jobs',
    description: 'The description jobs of the user',
  })
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiProperty({
    example: ' userId',
    description: 'userId',
  })
  @IsNotEmpty()
  @IsString()
  userId: string;
}