import { ApiProperty } from '@nestjs/swagger';

export class DeleteTasksResponse {
  @ApiProperty({
    example: 1,
    description: 'Number of removed tasks',
  })
  deletedCount: number;
}