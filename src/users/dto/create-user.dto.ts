// src/users/dto/create-user.dto.ts
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString,IsEmail } from 'class-validator';
export class CreateUserDto {

  @ApiProperty({
    example: 'mucharomtzaka123@gmail.com',
    description: 'The email of the user',
  })
  @IsEmail()
  @IsNotEmpty()
  @IsString()
  email: string;

  @ApiProperty({
    example: 'test',
    description: 'The username of the user',
  })
  @IsNotEmpty()
  @IsString()
  username: string;

  @ApiProperty({
        example: '12345678',
        description: 'The password of the user',
  })
  @IsNotEmpty()
  @IsString()
  password: string;
}
