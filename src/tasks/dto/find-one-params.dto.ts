import { IsString } from 'class-validator';

export class FindOneParams {
  @IsString()
  taskId: string;
}